<p align="center">
  <a href="https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation">
    <img src="https://assets.gentoo.org/tyrian/site-logo.svg" alt="Logo" width="384">
    <span style="margin-left: auto;width:100%">... install</span>
  </a>
</p>

### Creating bootable media 

[https://distfiles.gentoo.org/releases/amd64/autobuilds/\<timestamp\>/install-amd64-minimal-\<timestamp\>.iso](https://distfiles.gentoo.org/releases/amd64/autobuilds)

```sh
$ wget https://distfiles.gentoo.org/releases/amd64/autobuilds/20240324T164906Z/install-amd64-minimal-20240324T164906Z.iso
$ dd if=install-amd64-minimal-<timestamp>.iso of=/dev/<device> bs=4096 status=progress
$ sync
```

### Testing the network

```sh
$ ping -c 3 1.1.1.1
```

### Partitioning

| Device path (sysfs) | Mount point | File system | Description                |
|---------------------|-------------|-------------|----------------------------|
| /dev/nvme0n1p1      | /boot       | vfat        | EFI system partition (ESP) |
| /dev/nvme0n1p2      | N/A.        | swap        | Swap partition             |
| /dev/nvme0n1p3      | /           | btrfs       | Root partition             |

```sh
$ wipefs -a /dev/nvme0n1

$ cfdisk /dev/nvme0n1
$ # GPT Partition Table
$ # * 512M EFI   System
$ # * 8G   Linux Swap
$ # * Rest Linux System

$ mkfs.vfat -F 32 /dev/nvme0n1p1
$ mkswap /dev/nvme0n1p2
$ swapon /dev/nvme0n1p2
$ mkfs.btrfs -f /dev/nvme0n1p3

$ mkdir -p /mnt/gentoo
$ mount /dev/nvme0n1p3 /mnt/gentoo
$ mkdir -p /mnt/gentoo/boot/EFI
$ mount /dev/nvme0n1p1 /mnt/gentoo/boot
```

### Stage 3

[https://distfiles.gentoo.org/releases/amd64/autobuilds/\<timestamp\>/stage3-amd64-openrc-\<timestamp\>.tar.xz](https://distfiles.gentoo.org/releases/amd64/autobuilds)

```sh
cd /mnt/gentoo
links https://distfiles.gentoo.org/releases/amd64/autobuilds
tar xpvf stage3-*.tar.gz --xattrs-include='*.*' --numeric-owner
rm stage3-*.tar.xz
```

### Configuring compile options

```sh
nano /mnt/gentoo/etc/portage/make.conf

COMMON_FLAGS="-march=native -O2 -pipe"

USE="-examples -test -telemetry -systemd lzo vaapi wayland pipewire vulkan"

# min(RAM/2GB, $nproc)
MAKEOPTS="-j16 -l16"

L10N="de en-US ja"

ACCEPT_KEYWORDS="~amd64"

ACCEPT_LICENSE="*"

GENTOO_MIRRORS=""

VIDEO_CARDS="amdgpu radeonsi"
```

```sh
$ emerge -a app-portage/cpuid2cpuflags
$ echo "*/* $(cpuid2cpuflags)" > /etc/portage/package.use/00cpu-flags
```

### Chrooting

```sh
cp -L /etc/resolv.conf /mnt/gentoo/etc/

mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
mount --bind /run /mnt/gentoo/run
mount --make-slave /mnt/gentoo/run

chroot /mnt/gentoo /bin/bash
source /etc/profile
export PS1="(chroot) ${PS1}"
```

### Portage

```sh
$ mkdir -p /etc/portage/repos.conf
$ cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf
$ emerge-webrsync
$ eselect profile list
$ eselect profile set <nr>
```

#### Updating the @world set

```sh
$ emerge -avuDN @world
$ emerge -ac
```

### Timezone

```sh
$ ls -l /usr/share/zoneinfo/*
$ echo "Europe/Vienna" > /etc/timezone
$ emerge --config sys-libs/timezone-data
```

### Locale

```sh
$ nano /etc/locale.gen
$ locale-gen

$ eselect locale list
$ eselect locale set <nr>

$ env-update && source /etc/profile && export PS1="(chroot) ${PS1}"
```

### Firmware/Microcode

```sh
$ emerge -a sys-kernel/linux-firmware
```

### Kernel configuration and compilation

```sh
$ emerge -a sys-kernel/installkernel sys-kernel/gentoo-sources sys-kernel/genkernel

$ # /boot/EFI/Gentoo/initramfs-x.y.z-gentoo-dist.img
$ # /boot/EFI/Gentoo/kernel-x.y.z-gentoo-dist.efi

$ eselect kernel list
$ eselect kernel set 1

$ genkernel --menuconfig --btrfs all
```

#### Kernel upgrade

```sh
cd /usr/src/linux

# If previous kernel had CONFIG_IKCONFIG_PROC enabled.
zcat /proc/config.gz > /usr/src/linux/.config
# In the kernel directory of the current-running kernel
cp /usr/src/linux-X.X.X-gentoo/.config /usr/src/linux/

genkernel --menuconfig --btrfs all

installkernel 6.8.1-gentoo-x86_64 vmlinuz-6.8.1-gentoo-x86_64 System.map-6.8.1-gentoo-x86_64
```

### efibootmgr (should be automatically configured by installkernel)

```sh
emerge -a sys-boot/efibootmgr

mkdir -p /boot/EFI/Gentoo 
cp /boot/vmlinuz-* /boot/EFI/Gentoo/bzImage.efi 

efibootmgr --create --disk /dev/sda --part 1 --label "gentoo" --loader "\efi\gentoo\bzImage.efi"

efibootmgr -c -d /dev/sda -p 1 -L "Gentoo 6.8.1" -l "\efi\gentoo\vmlinuz-6.8.1-gentoo-x86_64.efi" -u "initrd=\efi\gentoo\initramfs-6.8.1-gentoo-x86_64.img"

efibootmgr -c -d /dev/nvme0n1 -p 1 -L "Gentoo 6.8.1" -l "\EFI\gentoo\vmlinuz-6.8.1-gentoo-x86_64.efi" -u "root=/dev/nvme0n1p2 initrd=\EFI\gentoo\initramfs-6.8.1-gentoo-x86_64.img" --gpt --write-signature
```

### fstab

```sh
$ nano /etc/fstab

# <file system>        <dir>         <type>    <options>             <dump> <pass>
/dev/nvme0n1p3         /             btrfs     defaults,noatime      0      1
/dev/nvme0n1p2         swap          swap      defaults              0      0
/dev/nvme0n1p1         /boot         vfat      defaults,umask=0077   0      2
/dev/nvme1n1p1         /mnt/games    btrfs     defaults,noatime      0      2
/dev/sda               /mnt/storage  btrfs     defaults,noatime      0      2
```

### Hostname

```sh
$ echo "gentoo" > /etc/hostname
```

### Network

```sh
$ emerge -a net-misc/dhcpcd
$ rc-update add dhcpcd default
$ rc-service dhcpcd start
```

#### Hosts

```sh
$ nano /etc/hosts
$ # 127.0.0.1 gentoo
```

### Root password

```sh
$ passwd
```

### sudo

```sh
$ emerge -a app-admin/sudo
$ EDITOR=nano visudo /etc/sudoers
$ # %wheel ALL=(ALL:ALL) ALL
```

### Keymaps

```sh
$ nano /etc/conf.d/keymaps
```

### System logger

```sh
$ emerge -a app-admin/sysklogd
$ rc-update add sysklogd default
```

### Cron daemon

```sh
$ emerge -a sys-process/cronie
$ rc-update add cronie default
```

### Shell completion

```sh
$ emerge -a app-shells/bash-completion
```

### Time synchronization

```sh
$ emerge -a net-misc/chrony
$ rc-update add chronyd default
```

### Filesystem tools

```sh
$ emerge -a sys-fs/btrfs-progs sys-fs/dosfstools sys-block/io-scheduler-udev-rules
```

#### Resize btrfs file system

```sh
btrfs filesystem resize max /path/to/drive
```

### Rebooting the system

```sh
(chroot) livecd # exit
$ umount -l /mnt/gentoo/dev{/shm,/pts,}
$ umount -R /mnt/gentoo
$ reboot
```

# Misc

### Adding a user

```sh
$ useradd -m -G users,wheel,audio -s /bin/bash <user>
$ passwd <user>
```

### AppImages

```sh
$ emerge -a sys-fs/fuse:0
```

### Update System

```sh
$ emaint -a sync
$ emerge -avuDN @world
$ emerge -ac
```

### Discord

```sh
emerge -a net-im/discord
```

# Local ebuild repository

```sh
eselect repository create local
cd /var/db/repos/local/
git init
git add .
git commit -m "+ <pkg>"

mkdir -p /var/db/repos/local/net-im/discord
cd /var/db/repos/local/net-im/discord
cp /var/db/repos/gentoo/net-im/discord/* .
cp discord-0.0.46.ebuild discord-0.0.47.ebuild

chown -R portage:portage /var/db/repos/local
pkgdev manifest
emerge <pkg>
```

# Gaming

### [Steam](https://wiki.gentoo.org/wiki/Steam)

CONFIG_COMPAT_32BIT_TIME
CONFIG_INPUT_UINPUT
USER_NS

```sh
emerge -an app-eselect/eselect-repository
eselect repository enable steam-overlay
emerge --sync

$ vim /etc/portage/package.use/steam

$ vim /etc/portage/package.accept_keywords/steam
$ # */*::steam-overlay
$ # games-util/game-device-udev-rules
$ # sys-libs/libudev-compat

$ emerge -a games-util/steam-launcher
```

### Bottles

### Gamescope

```sh
$ emerge -a gui-wm/gamescope
```

# Development

### git (required for steam-overlay)

```sh
$ emerge -a dev-vcs/git
```

### clang

### Visual Studio Code

```sh
emerge -a app-editors/vscode
```

#### Disable Telemetry

```sh
File -> Preferences -> Settings -> "telemetry.telemetryLevel": "off"
```
